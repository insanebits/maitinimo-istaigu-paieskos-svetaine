<?php
$default_value = 'Antradienio pietų meniu:

Morkų imbierinė sriuba su apelsinais /7.00Lt /2.02€
Pomidorų sriuba su ryžiais /6.00Lt /1.73€
Naminis vištos sultinys su pyragėliu /12.00Lt /3.47€
Naminis jaučio krūtininės sultinys /10.00Lt /2.89€

Sodelio kibinai su aviena /7.00Lt /2.02€
Jaučio befstrogenas /19.00Lt /5.50€
Kepta šviežia skumbrė su žolelėmis, pomidorais, šviežios salotos, skrudinta ciabatta /18.00Lt /5.21€
Itališki mėsos kukuliai, apjuosti rūkyta šonine, šviežių pomidorų padažas, Penne Rigatte, tarkuotas
kietas sūris /15.00Lt / 4.34€
Pasta ricca su briuselio kopūstais, špinatais, grietinėle ir kietuoju sūriu /13.00Lt /3.76€

Špinatų salotos su feta, šviežiais agurkais ir mandarinais /16.00Lt /4.63€
Cezario salotos /11.00Lt /3.18€
Cezario salotos su višta /14.00Lt /4.05€';

if(isset($_POST) && !empty($_POST))
{
    $file =  file($_FILES["csv"]["tmp_name"]);
    $titles = str_getcsv(array_shift($file));

    if(!empty($titles))
    {
        $default_value = '';
    }

    $data = array();
    foreach($titles as $node)
    {
        $data[] = str_getcsv(array_shift($file));
    }
    foreach ($titles as $k => $t) {
        $default_value .= PHP_EOL . $t . PHP_EOL;
        foreach ($data as $pos => $val) {
            $default_value .= !empty($data[$pos][$k]) ? $data[$pos][$k] . PHP_EOL : '';
        }
    }

    $default_value = trim($default_value);
}
?>

<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="css/bootstrap.min.css">

    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Maitinimo įstaigų paieškos sistema</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <p class="navbar-text navbar-right"><a href="./index.html" class="navbar-link">Atsijungti(Vartotojas)</a>
            </p>
        </div>
        <!--/.navbar-collapse -->
    </div>
</nav>

<div class="container">
    <div class="container col-md-6 col-md-offset-3">
        <div class="page-header text-center">
            <h1>Maitinimo įstaigos</h1>
        </div>
    </div>

    <div class="row">
        <div class="container col-md-6 col-md-offset-3">
            <table class="table">
                <tr>
                    <th>ID</th>
                    <th>Pavadinimas</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Čili pica "Akropolio"</td>
                    <td>
                        <a href="#" data-toggle="modal" data-target="#myModalMeniu">
                           Meniu
                        </a>
                    </td>
                    <td>
                        <a href="#" data-toggle="modal" data-target="#myModal3">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </a>
                    </td>
                    <td><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></i></td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Čili pica "Baršausko"</td>
                    <td>
                        <a href="#" data-toggle="modal" data-target="#myModalMeniu">
                            Meniu
                        </a>
                    </td>
                    <td>
                        <a href="#" data-toggle="modal" data-target="#myModal4">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </a>
                    </td>
                    <td><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></i></td>
                </tr>
            </table>
            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal2">Kurti
                įstaigą
            </button>
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span
                                    aria-hidden="true">&times;</span><span class="sr-only">Uždaryti</span></button>
                            <h4 class="modal-title" id="myModalLabel2">Kurti maitinimo įstaigą</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label for="inputPavadinimas" class="col-sm-2 control-label">Pavadinmas</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputPavadinimas"
                                               placeholder="Įmonės pavadinimas">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputKordinates" class="col-sm-2 control-label">Adresas</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputKordinates"
                                               placeholder="Pilnas įmonės adresas">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Uždaryti</button>
                            <button type="button" class="btn btn-primary">Kurti</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span
                                    aria-hidden="true">&times;</span><span class="sr-only">Uždaryti</span></button>
                            <h4 class="modal-title" id="myModalLabel3">Redaguoti maitinimo įstaigą</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label for="inputPavadinimasR" class="col-sm-2 control-label">Pavadinmas</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputPavadinimasR"
                                               placeholder="Įmonės pavadinimas" value='Čili pica "Akropolio"'>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputKordinatesR" class="col-sm-2 control-label">Adresas</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputKordinatesR"
                                               placeholder="Pilnas įmonės adresas" value='K.Mindaugo g.49, Kaunas'>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Uždaryti</button>
                            <button type="button" class="btn btn-primary">Saugoti</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span
                                    aria-hidden="true">&times;</span><span class="sr-only">Uždaryti</span></button>
                            <h4 class="modal-title" id="myModalLabel4">Redaguoti maitinimo įstaigą</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label for="inputPavadinimasR2" class="col-sm-2 control-label">Pavadinmas</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputPavadinimasR2"
                                               placeholder="Įmonės pavadinimas" value='Čili pica "Baršausko"'>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputKordinatesR2" class="col-sm-2 control-label">Adresas</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputKordinatesR2"
                                               placeholder="Pilnas įmonės adresas" value='Baršausko g. 66a, Kaunas'>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Uždaryti</button>
                            <button type="button" class="btn btn-primary">Saugoti</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="myModalMeniu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span
                                    aria-hidden="true">&times;</span><span class="sr-only">Uždaryti</span></button>
                            <h4 class="modal-title" id="myModalLabelMeniu">Pridėti meniu</h4>
                        </div>
                        <div class="modal-body">
                            <textarea rows="15" style="width: 100%;"><?php echo $default_value; ?></textarea>
                        </div>
                        <div class="modal-footer">
                            <form method="post" style="display: inline-block; margin-right: 5px;" enctype="multipart/form-data">
                                    <span class="btn btn-success btn-file">
                                    Įkelti csv <input type="file" name="csv">
                                    </span>

                                <button type="button"  class="btn btn-default" data-dismiss="modal">Uždaryti</button>
                                <button type="submit" name="save" class="btn btn-primary">Išsaugoti</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<hr>

<div class="container">
    <footer class="text-center">
        <p>&copy; Neveluosim 2014</p>
    </footer>
</div>
<!--container-->        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
</body>
</html>