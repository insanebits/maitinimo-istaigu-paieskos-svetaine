
// initialize select2 for native support of select tag
jQuery(document).ready(function(){
    jQuery('select.select2').select2();
    jQuery('#type-filter').select2({
        multiple : true,
        data: [
            {text:"Kavinės", id : 1},
            {text:"Restoranai", id : 2},
            {text:"Greitas maistas", id :3},
            {text:"Picerijos", id : 4},
        ]
    });

    jQuery('#distance-filter').select2({
        multiple : true,
        data: [
            {text:"500m", id : 1},
            {text:"1km", id : 2},
            {text:"5km", id :3},
            {text:"10km", id : 4},
        ]
    });

    jQuery('#price-filter').select2({
        multiple : true,
        data: [
            {text:"0 - 10 Lt", id : 1},
            {text:"10 - 20 Lt", id : 2},
            {text:"20 - 50 Lt", id :3},
            {text:"50 - 100 Lt", id : 4},
        ]
    });
});
